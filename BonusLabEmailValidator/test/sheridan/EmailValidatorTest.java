package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class EmailValidatorTest {
	
	@Test
	public void emailFormatRegularTest() {
		assertTrue("Email should be in <account>@<domain>.<extension>.", EmailValidator.isValidEmail("saundha@sheridancollege.ca"));
	}

	@Test
	public void emailSymbolRegularTest() {
		assertTrue("Email should contain only one @ symbol.", EmailValidator.isValidEmail("saundha@sheridancollege.ca"));
	}
	
	@Test
	public void emailSymbolExceptionTest() {
		assertFalse("Invalid email.", EmailValidator.isValidEmail("saundha"));
	}
	
	@Test
	public void emailAccountNameRegularTest() {
		assertTrue("Email account name shoul contain at least 3 alpha-characters.", EmailValidator.isValidEmail("saundha@sheridancollege.ca"));
	}
	
	@Test
	public void emailAccountNameExceptionTest() {
		assertFalse("Invalid email.", EmailValidator.isValidEmail("@sheridancollege.ca"));
	}
	@Test
	public void emailAccountNameBoundryInTest() {
		assertTrue("Email account name should contain at least 3 alpha-characters.", EmailValidator.isValidEmail("sau@sheridancollege.ca"));
	}
	@Test
	public void emailAccountNameBoundryOutTest() {
		assertFalse("Email account name should contain at least 3 alpha-characters.", EmailValidator.isValidEmail("sa@sheridancollege.ca"));
	}
	
	@Test
	public void emailDomainNameRegularTest() {
		assertTrue("Email domain name should contain at least 3 alpha-characters or numbers.", EmailValidator.isValidEmail("saundha@sheridancollege.ca"));
	}
	
	@Test
	public void emailDomainNameExceptionTest() {
		assertFalse("Invalid email.", EmailValidator.isValidEmail("saundha@.ca"));
	}
	@Test
	public void emailDomainNameBoundryInTest() {
		assertTrue("Email domain name should contain at least 3 alpha-characters or numbers.", EmailValidator.isValidEmail("saundha@she.ca"));
	}
	@Test
	public void emailDomainNameBoundryOutTest() {
		assertFalse("Email domain name should contain at least 3 alpha-characters or numbers.", EmailValidator.isValidEmail("saundha@sh.ca"));
	}
	
	@Test
	public void emailExtensionRegularTest() {
		assertTrue("Email extension should contain 2 or more alpha-characters.", EmailValidator.isValidEmail("saundha@sheridancollege.com"));
	}
	
	@Test
	public void emailExtensionExceptionTest() {
		assertFalse("Invalid email.", EmailValidator.isValidEmail("saundha@sheridancollege."));
	}
	@Test
	public void emailExtensionBoundryInTest() {
		assertTrue("Email extension should contain 2 or more alpha-characters.", EmailValidator.isValidEmail("saundha@sheridancollege.ca"));
	}
	@Test
	public void emailExtensionBoundryOutTest() {
		assertFalse("Email extension should contain 2 or more alpha-characters.", EmailValidator.isValidEmail("saundha@sheridancollege.c"));
	}

}
